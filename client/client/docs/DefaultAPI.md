# \DefaultAPI

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**CreateUserApiV2UsersPost**](DefaultAPI.md#CreateUserApiV2UsersPost) | **Post** /api/v2/users/ | Create User
[**GetApplicationsApiV2AppGet**](DefaultAPI.md#GetApplicationsApiV2AppGet) | **Get** /api/v2/app | Get Applications
[**GetResultApiV2TaskTaskIdResultGet**](DefaultAPI.md#GetResultApiV2TaskTaskIdResultGet) | **Get** /api/v2/task/{task_id}/result | Get Result
[**GetStatusApiV2TaskTaskIdStatusGet**](DefaultAPI.md#GetStatusApiV2TaskTaskIdStatusGet) | **Get** /api/v2/task/{task_id}/status | Get Status
[**SubmitJobApiV2TaskPost**](DefaultAPI.md#SubmitJobApiV2TaskPost) | **Post** /api/v2/task | Submit Job
[**ValidateApiV2ValidateRegistrationCodeGet**](DefaultAPI.md#ValidateApiV2ValidateRegistrationCodeGet) | **Get** /api/v2/validate/{registration_code} | Validate



## CreateUserApiV2UsersPost

> User CreateUserApiV2UsersPost(ctx).UserCreate(userCreate).Execute()

Create User

### Example

```go
package main

import (
	"context"
	"fmt"
	"os"
	openapiclient "github.com/GIT_USER_ID/GIT_REPO_ID"
)

func main() {
	userCreate := *openapiclient.NewUserCreate("Name_example", "Email_example") // UserCreate | 

	configuration := openapiclient.NewConfiguration()
	apiClient := openapiclient.NewAPIClient(configuration)
	resp, r, err := apiClient.DefaultAPI.CreateUserApiV2UsersPost(context.Background()).UserCreate(userCreate).Execute()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error when calling `DefaultAPI.CreateUserApiV2UsersPost``: %v\n", err)
		fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
	}
	// response from `CreateUserApiV2UsersPost`: User
	fmt.Fprintf(os.Stdout, "Response from `DefaultAPI.CreateUserApiV2UsersPost`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiCreateUserApiV2UsersPostRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **userCreate** | [**UserCreate**](UserCreate.md) |  | 

### Return type

[**User**](User.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetApplicationsApiV2AppGet

> ListAppsResponse GetApplicationsApiV2AppGet(ctx).Execute()

Get Applications

### Example

```go
package main

import (
	"context"
	"fmt"
	"os"
	openapiclient "github.com/GIT_USER_ID/GIT_REPO_ID"
)

func main() {

	configuration := openapiclient.NewConfiguration()
	apiClient := openapiclient.NewAPIClient(configuration)
	resp, r, err := apiClient.DefaultAPI.GetApplicationsApiV2AppGet(context.Background()).Execute()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error when calling `DefaultAPI.GetApplicationsApiV2AppGet``: %v\n", err)
		fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
	}
	// response from `GetApplicationsApiV2AppGet`: ListAppsResponse
	fmt.Fprintf(os.Stdout, "Response from `DefaultAPI.GetApplicationsApiV2AppGet`: %v\n", resp)
}
```

### Path Parameters

This endpoint does not need any parameter.

### Other Parameters

Other parameters are passed through a pointer to a apiGetApplicationsApiV2AppGetRequest struct via the builder pattern


### Return type

[**ListAppsResponse**](ListAppsResponse.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetResultApiV2TaskTaskIdResultGet

> ResultResponse GetResultApiV2TaskTaskIdResultGet(ctx, taskId).Execute()

Get Result

### Example

```go
package main

import (
	"context"
	"fmt"
	"os"
	openapiclient "github.com/GIT_USER_ID/GIT_REPO_ID"
)

func main() {
	taskId := "taskId_example" // string | 

	configuration := openapiclient.NewConfiguration()
	apiClient := openapiclient.NewAPIClient(configuration)
	resp, r, err := apiClient.DefaultAPI.GetResultApiV2TaskTaskIdResultGet(context.Background(), taskId).Execute()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error when calling `DefaultAPI.GetResultApiV2TaskTaskIdResultGet``: %v\n", err)
		fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
	}
	// response from `GetResultApiV2TaskTaskIdResultGet`: ResultResponse
	fmt.Fprintf(os.Stdout, "Response from `DefaultAPI.GetResultApiV2TaskTaskIdResultGet`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**taskId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetResultApiV2TaskTaskIdResultGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**ResultResponse**](ResultResponse.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## GetStatusApiV2TaskTaskIdStatusGet

> StatusResponse GetStatusApiV2TaskTaskIdStatusGet(ctx, taskId).Execute()

Get Status

### Example

```go
package main

import (
	"context"
	"fmt"
	"os"
	openapiclient "github.com/GIT_USER_ID/GIT_REPO_ID"
)

func main() {
	taskId := "taskId_example" // string | 

	configuration := openapiclient.NewConfiguration()
	apiClient := openapiclient.NewAPIClient(configuration)
	resp, r, err := apiClient.DefaultAPI.GetStatusApiV2TaskTaskIdStatusGet(context.Background(), taskId).Execute()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error when calling `DefaultAPI.GetStatusApiV2TaskTaskIdStatusGet``: %v\n", err)
		fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
	}
	// response from `GetStatusApiV2TaskTaskIdStatusGet`: StatusResponse
	fmt.Fprintf(os.Stdout, "Response from `DefaultAPI.GetStatusApiV2TaskTaskIdStatusGet`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**taskId** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiGetStatusApiV2TaskTaskIdStatusGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**StatusResponse**](StatusResponse.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## SubmitJobApiV2TaskPost

> SubmitResponse SubmitJobApiV2TaskPost(ctx).SubmitRequest(submitRequest).Execute()

Submit Job

### Example

```go
package main

import (
	"context"
	"fmt"
	"os"
	openapiclient "github.com/GIT_USER_ID/GIT_REPO_ID"
)

func main() {
	submitRequest := *openapiclient.NewSubmitRequest([]string{"Command_example"}) // SubmitRequest | 

	configuration := openapiclient.NewConfiguration()
	apiClient := openapiclient.NewAPIClient(configuration)
	resp, r, err := apiClient.DefaultAPI.SubmitJobApiV2TaskPost(context.Background()).SubmitRequest(submitRequest).Execute()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error when calling `DefaultAPI.SubmitJobApiV2TaskPost``: %v\n", err)
		fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
	}
	// response from `SubmitJobApiV2TaskPost`: SubmitResponse
	fmt.Fprintf(os.Stdout, "Response from `DefaultAPI.SubmitJobApiV2TaskPost`: %v\n", resp)
}
```

### Path Parameters



### Other Parameters

Other parameters are passed through a pointer to a apiSubmitJobApiV2TaskPostRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **submitRequest** | [**SubmitRequest**](SubmitRequest.md) |  | 

### Return type

[**SubmitResponse**](SubmitResponse.md)

### Authorization

[APIKeyHeader](../README.md#APIKeyHeader)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ValidateApiV2ValidateRegistrationCodeGet

> ValidationResponse ValidateApiV2ValidateRegistrationCodeGet(ctx, registrationCode).Execute()

Validate

### Example

```go
package main

import (
	"context"
	"fmt"
	"os"
	openapiclient "github.com/GIT_USER_ID/GIT_REPO_ID"
)

func main() {
	registrationCode := "registrationCode_example" // string | 

	configuration := openapiclient.NewConfiguration()
	apiClient := openapiclient.NewAPIClient(configuration)
	resp, r, err := apiClient.DefaultAPI.ValidateApiV2ValidateRegistrationCodeGet(context.Background(), registrationCode).Execute()
	if err != nil {
		fmt.Fprintf(os.Stderr, "Error when calling `DefaultAPI.ValidateApiV2ValidateRegistrationCodeGet``: %v\n", err)
		fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
	}
	// response from `ValidateApiV2ValidateRegistrationCodeGet`: ValidationResponse
	fmt.Fprintf(os.Stdout, "Response from `DefaultAPI.ValidateApiV2ValidateRegistrationCodeGet`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**registrationCode** | **string** |  | 

### Other Parameters

Other parameters are passed through a pointer to a apiValidateApiV2ValidateRegistrationCodeGetRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


### Return type

[**ValidationResponse**](ValidationResponse.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

