import requests
import time
import sys
from pathlib import Path
from base64 import b64encode, b64decode
import os

# Reference Python implementation of a REX client.
#
# Requires the following environment variables to be set:
#
#  REX_SERVER: URL for the server (e.g. "http://localhost:8000")
#  REX_API_KEY: Api key - may provide normal user or admin user
#                         privileges.
#  REX_ADMIN_API_KEY: Optional api key specifically for admin access.


def check_response(response: requests.Response) -> None:
    try:
        response.raise_for_status()
    except requests.RequestException as e:
        # handle all errors here
        print(f"Response error: {e}")
        if 'detail' in response.json():
            print(f'detail: {response.json()["detail"]}')
        exit(1)
    return


base_url = os.environ.get('REX_SERVER') + '/api/v2'
if base_url[:4] != 'http':
    base_url = 'https://' + base_url
    
api_key = os.environ.get('REX_API_KEY')
admin_api_key = os.environ.get('REX_ADMIN_API_KEY')

headers = {'x-api-key': api_key}
admin_headers = {'x-api-key': admin_api_key}
if sys.argv[1] == '--info':
    # Info about the REX client/server
    print(f'REX base URL: {base_url}')
    print(f'Rex API key: {api_key}')

elif sys.argv[1] == '--list':
    # List available applications
    response = requests.get(f'{base_url}/app', headers=headers)
    check_response(response)
    appnames = [a['name'] for a in response.json()['apps']]
    for name in sorted(appnames):
        print(name)
elif sys.argv[1] == '--list-users':
    # List registerd users - requires admin privileges
    response = requests.get(f'{base_url}/users/', headers=admin_headers)
    check_response(response)
    for user in response.json():
        print(user)
elif sys.argv[1] == '--register':
    # Register for REX - one-time validation key sent by email
    name = input('Username: ')
    email = input('Email: ')
    data = {'name': name, 'email': email}
    response = requests.post(f'{base_url}/users', json=data, headers=headers)
    check_response(response)
    print(response.json())
elif sys.argv[1] == '--validate':
    # Validate a new user - send validation code, receive api key
    registration_key = sys.argv[2]
    response = requests.get(f'{base_url}/validate/{registration_key}')
    check_response(response)
    print('Success - here is your api key:')
    print(response.json()['apikey'])
    print('Keep it safe - if you lose it you will have to re-register')
else:
    # Run a task on the rex server
    cmd = sys.argv[1:]
    inputfiles = []
    for filename in sys.argv[2:]:
        file = Path(filename)
        if file.is_file():
            content = b64encode(file.read_bytes()).decode("ascii")
            inputfiles.append({'name': file.name,
                               'content':  content})

    data = {'command': cmd, 'inputfiles': inputfiles}

    response = requests.post(f'{base_url}/task', json=data, headers=headers)
    check_response(response)
    key = response.json()['key']
    url = f'{base_url}/task/{key}/status'
    response2 = requests.get(url, headers=headers)
    check_response(response2)
    status = response2.json()['status']
    while status == 'pending':
        time.sleep(1)
        response2 = requests.get(url, headers=headers)
        check_response(response2)
        status = response2.json()['status']

    url = f'{base_url}/task/{key}/result'
    response3 = requests.get(url, headers=headers)
    check_response(response3)
    result = response3.json()['task_result']
    if len(result['outputfiles']) > 0:
        outfiles = result['outputfiles']
        for file in outfiles:
            content = b64decode(file['content'])
            f = Path(file['name'])
            f.write_bytes(content)

    print(result['stdout'])
    if result['returncode'] != 0:
        print(result['stderr'])
        exit(result['returncode'])
