from fastapi import FastAPI, Request
from fastapi import Security, HTTPException, status
from fastapi.security import APIKeyHeader


from contextlib import asynccontextmanager

from dotenv import load_dotenv

from time import time

from email_validator import validate_email, EmailNotValidError

import uuid
import os
import random
from pathlib import Path

from .db import SQLDB
from .taskrunner import TaskRunner
from .mailing import Mailer
from .models import (User,
                     UserCreate,
                     SubmitRequest,
                     SubmitResponse,
                     StatusResponse,
                     ResultResponse,
                     ListAppsResponse,
                     ValidationResponse
                     )

import logging

logging.basicConfig()
# Load environment variables
load_dotenv()

# Roles
PERMANENT = 1
ADMIN = 2
USER = 4
VALIDATED = 8

# Database instantiation
sqlite_file_name = os.environ.get("REX_SQLITE_DATABASE")
sqldb = SQLDB(sqlite_file_name)

# For api key checking functions:
api_key_header = APIKeyHeader(name="X-API-Key", auto_error=False)

# In-memory store for rate limiting
rate_limit_store = {}


def rate_limiter(request: Request):
    client_host = request.client.host
    current_time = time()
    if client_host not in rate_limit_store:
        rate_limit_store[client_host] = [current_time]
    else:
        # Remove timestamps older than 60 seconds
        rate_limit_store[client_host] = [t for t
                                         in rate_limit_store[client_host]
                                         if current_time - t < 60]

    if len(rate_limit_store[client_host]) > 1:
        raise HTTPException(status_code=429, detail="Rate limit exceeded")

    rate_limit_store[client_host].append(current_time)
    return client_host


def authorized_user(api_key: str = Security(api_key_header)):
    if api_key:
        user = sqldb.get_user_by_key(api_key)
        if user.roles & VALIDATED == VALIDATED:
            return user
    raise HTTPException(status_code=401, detail='Missing or invalid API key')


def authorized_admin(api_key: str = Security(api_key_header)):
    if api_key:
        admin = sqldb.get_user_by_key(api_key)
        if admin:
            if admin.roles & ADMIN == ADMIN:
                return admin
    raise HTTPException(status_code=401, detail='Missing or invalid API key')


def valid_email(user: UserCreate):
    try:
        emailinfo = validate_email(user.email, check_deliverability=True)
        email = emailinfo.normalized
    except EmailNotValidError:
        email = ""

    if email[-6:] == ".ac.uk":
        return email
    raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="not a valid *.ac.uk email address"
            )


mailer = Mailer()
task_runner = TaskRunner(2)


@asynccontextmanager
async def lifespan(app: FastAPI):
    # Things to be done at start-up:
    sqldb.create_db_and_tables()
    api_key = os.environ.get('REX_ADMIN_API_KEY')
    if api_key:
        sqldb.create_initial_admin(api_key)
    else:
        print('Error - REX_ADMIN_API_KEY not set')
        exit(1)
    bindirname = os.environ.get('REX_BIN_DIR')
    if bindirname:
        bindir = Path(bindirname)
        ignorefile = bindir / '.rexignore'
        if ignorefile.is_file():
            ignoredapps = ignorefile.read_text().split('\n')
        else:
            ignoredapps = []
        allapps = bindir.glob('*')
        apps = []
        for a in allapps:
            if a.name not in ignoredapps and a.is_file():
                apps.append(a.name)
        sqldb.initialize_applications(apps)
    else:
        print('Error - REX_BIN_DIR not set')
        exit(1)

    mailer.check()
    task_runner.startup()
    yield
    # things to be done at shutdown:
    task_runner.shutdown()


# App startup
app = FastAPI(lifespan=lifespan)


# Paths
# a) For admin use only:
@app.get("/api/v2/users/", include_in_schema=False)
def get_users(requester: User = Security(authorized_admin)):
    return sqldb.get_users()


@app.get("/api/v2/users/{id}", include_in_schema=False)
def app_get_user(id: int,
                 requester: User = Security(authorized_admin)):
    return sqldb.get_user_by_id(id)


@app.delete("/api/v2/users/{id}", include_in_schema=False)
def app_delete_user(id: int,
                    requester: User = Security(authorized_admin)):
    user = sqldb.get_user_by_id(id)
    if user.roles & USER == USER:
        result = sqldb.delete_user(id)
        return result
    raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail='user is admin'
        )


@app.get("/api/v2/admins/", include_in_schema=False)
def app_get_admins(requester: User = Security(authorized_admin)):
    users = sqldb.get_users()
    return [a for a in users if a.roles & ADMIN == ADMIN]


@app.get("/api/v2/admins/{id}", include_in_schema=False)
def app_get_admin(id: int,
                  requester: User = Security(authorized_admin)):
    admin = sqldb.get_user_by_id(id)
    if admin.roles & ADMIN == ADMIN:
        return admin
    raise HTTPException(
        status_code=status.HTTP_404_NOT_FOUND,
        detail="No such admin"
    )


@app.post("/api/v2/admins/", include_in_schema=False)
def app_create_admin(admin: UserCreate,
                     requester: User = Security(authorized_admin)):
    apikey = str(uuid.uuid4())
    roles = ADMIN + USER + VALIDATED
    update = {'roles': roles, 'apikey': apikey}
    db_admin = User.model_validate(admin, update=update)
    return sqldb.create_user(db_admin)


@app.delete("/api/v2/admins/{id}", include_in_schema=False)
def app_delete_admin(id: int,
                     requester: User = Security(authorized_admin)):
    user = sqldb.get_user_by_id(id)
    if user.roles & ADMIN == ADMIN:
        if user.roles & PERMANENT == 0:
            result = sqldb.delete_user(id)
            return result
        raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN,
                detail='admin is protected'
        )
    raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail='user is not admin'
        )


@app.patch("/api/v2/app/{app_name}", include_in_schema=False)
def app_increment_application(app_name: str,
                              requester: User = Security(authorized_admin)):
    apl = sqldb.get_application_by_name(app_name)
    apl.invocations += 1
    return sqldb.update_application(apl)


# Start taskrunner

@app.get("/api/v2/app",
         response_model=ListAppsResponse)
def get_applications(requester: User = Security(authorized_user)):
    apps = sqldb.get_applications()
    return {'apps': apps}


@app.post("/api/v2/users/",
          response_model=User)
def create_user(user: UserCreate,
                client_host=Security(rate_limiter)):
    # apikey = str(uuid.uuid4())
    email = valid_email(user)
    apikey = ''.join([str(random.randint(0, 9)) for i in range(6)])
    roles = USER
    update = {'roles': roles, 'apikey': apikey}
    db_user = User.model_validate(user, update=update)
    mailer.send_vc(user.email, apikey, dry_run=True)
    return sqldb.create_user(db_user)


@app.post("/api/v2/task",
          response_model=SubmitResponse)
async def submit_job(sr: SubmitRequest,
                     requester: User = Security(authorized_user)):
    args = sr.command
    apl = sqldb.get_application_by_name(args[0])
    bindir = Path(os.environ.get('REX_BIN_DIR'))
    if apl:
        args[0] = bindir / apl.name
        inputfiles = sr.inputfiles
        key = task_runner.submit(command=args,
                                 inputfiles=inputfiles)
        return {'key': key}
    raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail='application is not available'
        )


@app.get("/api/v2/task/{task_id}/status",
         response_model=StatusResponse)
async def get_status(task_id: str,
                     requester: User = Security(authorized_user)):
    status = task_runner.check_status(task_id)
    return {'status': status}


@app.get("/api/v2/task/{task_id}/result",
         response_model=ResultResponse)
async def get_result(task_id: str,
                     requester: User = Security(authorized_user)):
    return task_runner.retrieve(task_id)


@app.get("/api/v2/validate/{registration_code}",
         response_model=ValidationResponse)
async def validate(registration_code: str):
    # return {'apikey': registration_code}
    user = sqldb.get_user_by_key(registration_code)
    if user:
        if user.roles & VALIDATED != VALIDATED:
            user.roles += VALIDATED
            user.apikey = str(uuid.uuid4())
            user = sqldb.update_user(user)
            return {'apikey': user.apikey}
    raise HTTPException(
            status_code=status.HTTP_404_NOT_FOUND,
            detail='unrecognised registration code'
        )
