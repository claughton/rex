# ProcessResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Returncode** | Pointer to **int32** |  | [optional] [default to -1]
**Stdout** | Pointer to **string** |  | [optional] [default to ""]
**Stderr** | Pointer to **string** |  | [optional] [default to ""]
**Outputfiles** | Pointer to [**[]FileData**](FileData.md) |  | [optional] [default to []]

## Methods

### NewProcessResponse

`func NewProcessResponse() *ProcessResponse`

NewProcessResponse instantiates a new ProcessResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewProcessResponseWithDefaults

`func NewProcessResponseWithDefaults() *ProcessResponse`

NewProcessResponseWithDefaults instantiates a new ProcessResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetReturncode

`func (o *ProcessResponse) GetReturncode() int32`

GetReturncode returns the Returncode field if non-nil, zero value otherwise.

### GetReturncodeOk

`func (o *ProcessResponse) GetReturncodeOk() (*int32, bool)`

GetReturncodeOk returns a tuple with the Returncode field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetReturncode

`func (o *ProcessResponse) SetReturncode(v int32)`

SetReturncode sets Returncode field to given value.

### HasReturncode

`func (o *ProcessResponse) HasReturncode() bool`

HasReturncode returns a boolean if a field has been set.

### GetStdout

`func (o *ProcessResponse) GetStdout() string`

GetStdout returns the Stdout field if non-nil, zero value otherwise.

### GetStdoutOk

`func (o *ProcessResponse) GetStdoutOk() (*string, bool)`

GetStdoutOk returns a tuple with the Stdout field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStdout

`func (o *ProcessResponse) SetStdout(v string)`

SetStdout sets Stdout field to given value.

### HasStdout

`func (o *ProcessResponse) HasStdout() bool`

HasStdout returns a boolean if a field has been set.

### GetStderr

`func (o *ProcessResponse) GetStderr() string`

GetStderr returns the Stderr field if non-nil, zero value otherwise.

### GetStderrOk

`func (o *ProcessResponse) GetStderrOk() (*string, bool)`

GetStderrOk returns a tuple with the Stderr field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetStderr

`func (o *ProcessResponse) SetStderr(v string)`

SetStderr sets Stderr field to given value.

### HasStderr

`func (o *ProcessResponse) HasStderr() bool`

HasStderr returns a boolean if a field has been set.

### GetOutputfiles

`func (o *ProcessResponse) GetOutputfiles() []FileData`

GetOutputfiles returns the Outputfiles field if non-nil, zero value otherwise.

### GetOutputfilesOk

`func (o *ProcessResponse) GetOutputfilesOk() (*[]FileData, bool)`

GetOutputfilesOk returns a tuple with the Outputfiles field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetOutputfiles

`func (o *ProcessResponse) SetOutputfiles(v []FileData)`

SetOutputfiles sets Outputfiles field to given value.

### HasOutputfiles

`func (o *ProcessResponse) HasOutputfiles() bool`

HasOutputfiles returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


