from typing import Optional
from sqlmodel import Field, SQLModel
from datetime import datetime
from sqlalchemy import Column, TIMESTAMP, text


class Application(SQLModel, table=True):
    id: Optional[int] = Field(default=None, primary_key=True)
    name: str
    invocations: int
    last_invocation: datetime = Field(
        sa_column=Column(TIMESTAMP(timezone=True),
        server_onupdate=text("now()"),
        nullable=True
        )
    )

class UserBase(SQLModel):
    name: str = Field(index=True)
    email: str


class UserCreate(UserBase):  # used by front end
    pass


class User(UserBase, table=True):  # used by database
    id: Optional[int] = Field(default=None, primary_key=True)
    apikey: str
    roles: int


class FileData(SQLModel):
    name: str
    content: str


class SubmitRequest(SQLModel):
    command: list[str]
    inputfiles: Optional[list[FileData]] = []


class StatusResponse(SQLModel):
    status: str


class SubmitResponse(SQLModel):
    key: str


class ProcessResponse(SQLModel):
    returncode: int = -1
    stdout: str = ""
    stderr: str = ""
    outputfiles: list[FileData] = []


class ResultResponse(SQLModel):
    task_status: str
    task_result: ProcessResponse


class ListAppsResponse(SQLModel):
    apps: list[Application]


class ValidationResponse(SQLModel):
    apikey: str
