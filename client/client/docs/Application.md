# Application

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Id** | Pointer to **NullableInt32** |  | [optional] 
**Name** | **string** |  | 
**Invocations** | **int32** |  | 
**LastInvocation** | Pointer to **time.Time** |  | [optional] 

## Methods

### NewApplication

`func NewApplication(name string, invocations int32, ) *Application`

NewApplication instantiates a new Application object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewApplicationWithDefaults

`func NewApplicationWithDefaults() *Application`

NewApplicationWithDefaults instantiates a new Application object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetId

`func (o *Application) GetId() int32`

GetId returns the Id field if non-nil, zero value otherwise.

### GetIdOk

`func (o *Application) GetIdOk() (*int32, bool)`

GetIdOk returns a tuple with the Id field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetId

`func (o *Application) SetId(v int32)`

SetId sets Id field to given value.

### HasId

`func (o *Application) HasId() bool`

HasId returns a boolean if a field has been set.

### SetIdNil

`func (o *Application) SetIdNil(b bool)`

 SetIdNil sets the value for Id to be an explicit nil

### UnsetId
`func (o *Application) UnsetId()`

UnsetId ensures that no value is present for Id, not even an explicit nil
### GetName

`func (o *Application) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *Application) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *Application) SetName(v string)`

SetName sets Name field to given value.


### GetInvocations

`func (o *Application) GetInvocations() int32`

GetInvocations returns the Invocations field if non-nil, zero value otherwise.

### GetInvocationsOk

`func (o *Application) GetInvocationsOk() (*int32, bool)`

GetInvocationsOk returns a tuple with the Invocations field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInvocations

`func (o *Application) SetInvocations(v int32)`

SetInvocations sets Invocations field to given value.


### GetLastInvocation

`func (o *Application) GetLastInvocation() time.Time`

GetLastInvocation returns the LastInvocation field if non-nil, zero value otherwise.

### GetLastInvocationOk

`func (o *Application) GetLastInvocationOk() (*time.Time, bool)`

GetLastInvocationOk returns a tuple with the LastInvocation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetLastInvocation

`func (o *Application) SetLastInvocation(v time.Time)`

SetLastInvocation sets LastInvocation field to given value.

### HasLastInvocation

`func (o *Application) HasLastInvocation() bool`

HasLastInvocation returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


