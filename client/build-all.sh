#!/bin/bash
#
# Build rex for multiple platforms
#
package_name="rex"
source_file="rex.go"
platforms=("windows/amd64" "windows/386" "darwin/amd64" "darwin/arm64" "linux/386" "linux/amd64")
for platform in "${platforms[@]}"
do
    platform_split=(${platform//\// })
    GOOS=${platform_split[0]}
    GOARCH=${platform_split[1]}
    executable_name=$package_name'-'$GOOS'-'$GOARCH
    if [ $GOOS = "windows" ]; then
        executable_name+='.exe'
    fi
    env GOOS=$GOOS GOARCH=$GOARCH go build -o $executable_name $source_file
done

