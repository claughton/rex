from fastapi import Security, HTTPException, status
from fastapi.security import APIKeyHeader
from .db import check_user_api_key, check_admin_api_key

api_key_header = APIKeyHeader(name="X-API-Key")


def get_user(api_key_header: str = Security(api_key_header)):
    user = check_user_api_key(api_key_header)
    if user:
        return user
    user = check_admin_api_key(api_key_header)
    if user:
        return user
    raise HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Missing or invalid API key"
    )


def get_admin(api_key_header: str = Security(api_key_header)):
    admin = check_admin_api_key(api_key_header)
    if admin:
        return admin
    raise HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Missing or invalid API key"
    )
