import tempfile
import subprocess
from base64 import b64encode, b64decode
from pathlib import Path
from distributed import Client, wait
from fastapi import HTTPException, status

task_db = {}


def safe_path(path, root):
    rootpath = Path(root).resolve()
    path = Path(path).resolve()
    try:
        relpath = path.relative_to(rootpath)
        return relpath
    except ValueError:
        return None


def runit(command: list, inputfiles: list = []):
    """
    Run a command
    """
    with tempfile.TemporaryDirectory() as tmpdirname:
        tmpdir = Path(tmpdirname)
        inputfilenames = []
        for file in inputfiles:
            content = b64decode(file.content)
            f = tmpdir / file.name
            f.write_bytes(content)
            inputfilenames.append(file.name)

        result = subprocess.run(
            command,
            cwd=tmpdir,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE
        )

        r = result.__dict__
        r['stdout'] = result.stdout.decode()
        r['stderr'] = result.stderr.decode()

        files = tmpdir.rglob("*")
        outfiles = []
        for file in files:
            if file.is_file():
                if file.name not in inputfilenames:
                    content = b64encode(file.read_bytes()).decode('ascii')
                    outfiles.append({'name': file.name,
                                     'content': content})
    r['outputfiles'] = outfiles
    return r


class TaskRunner():

    def __init__(self, n_workers: str):
        self.n_workers = n_workers

    def startup(self):
        self.client = Client(n_workers=self.n_workers)

    def shutdown(self):
        self.client.shutdown()

    def submit(self, command, inputfiles={}):
        safe = True
        for f in command[1:]:
            sp = safe_path(f, '.')
            if not sp:
                safe = False
        for f in inputfiles:
            sp = safe_path(f.name, '.')
            if not sp:
                safe = False
        if not safe:
            raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN,
                detail="disallowed path in command"
            )

        future = self.client.submit(runit, command,
                                    inputfiles=inputfiles, pure=False)
        task_db[future.key] = future
        return future.key

    def check_status(self, key):
        if key not in task_db:
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail=f"Unknown job_id: {key}"
            )
        task = task_db[key]
        return task.status

    def retrieve(self, key):
        task = task_db[key]
        wait(task)
        result = {'task_status': task.status}

        if task.status == 'finished':
            result['task_result'] = task.result()
            del task_db[key]

        else:
            result['task_result'] = {'stderr': str(task.exception())}
        return result
