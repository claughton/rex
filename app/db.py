from fastapi import HTTPException, status
from sqlmodel import Session, SQLModel, create_engine, select
from .models import User, Application


# Roles
PERMANENT = 1
ADMIN = 2
USER = 4


class SQLDB():
    def __init__(self, sqlite_file_name="database.db"):
        sqlite_url = f"sqlite:///{sqlite_file_name}"
        connect_args = {"check_same_thread": False}
        self.engine = create_engine(sqlite_url,
                                    echo=False,
                                    connect_args=connect_args)

    def create_db_and_tables(self):
        SQLModel.metadata.create_all(self.engine)

    def create_initial_admin(self, apikey):
        users = self.get_users()
        for user in users:
            if user.name == 'admin':
                self.delete_user(user.id)
        admin = User(name='admin',
                     email='admin@rexserver.org',
                     roles=PERMANENT + ADMIN + USER,
                     apikey=apikey)
        self.create_user(admin)

    def get_user_by_id(self, id: int):
        with Session(self.engine) as session:
            user = session.get(User, id)
            if user:
                return user
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail="No such user"
            )

    def get_users(self):
        with Session(self.engine) as session:
            users = session.exec(select(User)).all()
            return users

    def create_user(self, user: User):
        with Session(self.engine) as session:
            usernames = session.exec(select(User.name)).all()
            if user.name not in usernames:
                session.add(user)
                session.commit()
                session.refresh(user)
                return user
            raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN,
                detail='username exists'
            )

    def update_user(self, user: User):
        with Session(self.engine) as session:
            existing_user = self.get_user_by_id(user.id)
            if existing_user:
                session.add(user)
                session.commit()
                session.refresh(user)
                return user
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail='no such user'
            )

    def delete_user(self, id: int):
        with Session(self.engine) as session:
            user = session.get(User, id)
            if user:
                session.delete(user)
                session.commit()
                return True
            raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN,
                detail='no such user'
            )

    def get_user_by_key(self, api_key: str):
        with Session(self.engine) as session:
            statement = select(User).where(User.apikey == api_key)
            results = session.exec(statement)
            user = results.first()
            return user

    def delete_all_applications(self):
        with Session(self.engine) as session:
            for apl in self.get_applications():
                session.delete(apl)
            session.commit()
        apls = self.get_applications()
        return apls

    def get_application_by_name(self, name: str):
        with Session(self.engine) as session:
            apl = session.exec(select(Application).where(
                Application.name == name)).first()
            return apl

    def get_applications(self):
        with Session(self.engine) as session:
            apls = session.exec(select(Application)).all()
            return apls

    def initialize_applications(self, appnames):
        apls = self.get_applications()
        for apl in apls:
            if apl.name not in appnames:
                self.delete_application(apl.id)

        current_apnames = [a.name for a in self.get_applications()]
        for name in appnames:
            if name not in current_apnames:
                Apl = Application(name=name, invocations=0)
                self.add_application(Apl)

    def add_application(self, apl: Application):
        with Session(self.engine) as session:
            existing_apl = self.get_application_by_name(apl.name)
            if not existing_apl:
                session.add(apl)
                session.commit()
                session.refresh(apl)
                return apl
            else:
                return existing_apl

    def update_application(self, apl: Application):
        with Session(self.engine) as session:
            existing_apl = self.get_application_by_name(apl.name)
            if existing_apl:
                session.add(apl)
                session.commit()
                session.refresh(apl)
                return apl
            raise HTTPException(
                status_code=status.HTTP_404_NOT_FOUND,
                detail='no such application'
            )

    def delete_application(self, id: int):
        with Session(self.engine) as session:
            apl = session.get(Application, id)
            if apl:
                session.delete(apl)
                session.commit()
                return True
            raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN,
                detail='no such application'
            )
