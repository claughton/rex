import smtplib
from email.mime.text import MIMEText
import os
from dotenv import load_dotenv

load_dotenv()


class Mailer():
    def __init__(self):
        self.mail_login = os.environ.get('REX_MAIL_LOGIN')
        if not self.mail_login:
            print('Error: REX_MAIL_LOGIN not set')
            exit(1)
        self.mail_password = os.environ.get('REX_MAIL_PASSWORD')
        if not self.mail_password:
            print('Error: REX_MAIL_PASSWORD not set')
            exit(1)
        self.mail_sender = os.environ.get('REX_MAIL_SENDER')
        if not self.mail_sender:
            print('Error: REX_MAIL_SENDER not set')
            exit(1)
        self.mail_server = os.environ.get('REX_MAIL_SERVER')
        if not self.mail_server:
            print('Error: REX_MAIL_SERVER not set')
            exit(1)
        self.mail_port = os.environ.get('REX_MAIL_PORT')
        if not self.mail_port:
            self.mail_port = 587

        self.message_template = """\
Hi,

Thanks you for registering for REX.

Use:
rex --validate {}

To retrieve your API key.

Note this code can only be used once!

Regards,
The REX Adminstrators"""

    def check(self):
        with smtplib.SMTP(self.mail_server, self.mail_port) as server:
            server.starttls()  # Secure the connection
            server.login(self.mail_login, self.mail_password)

    def send_vc(self, user_email, validation_code, dry_run=False):
        message_text = self.message_template.format(validation_code)
        message = MIMEText(message_text, "plain")
        message["Subject"] = "Here is your REX validation code"
        message["From"] = self.mail_sender
        message["To"] = user_email
        if dry_run:
            print(message.as_string())
        else:
            with smtplib.SMTP(self.mail_server, self.mail_port) as server:
                server.starttls()  # Secure the connection
                server.login(self.mail_login, self.mail_password)
                server.sendmail(self.mail_sender,
                                user_email,
                                message.as_string())
