package main

import (
	"bufio"
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"slices"
	"strings"
	"time"

	openapiclient "rex.charlielaughton.com/client"
)


func main() {
	config := loadConfig()
	if config.RexServer == "" {
		log.Fatal("Error: no REX_SERVER environment variable found!")
	}
        valid_url := strings.HasPrefix(config.RexServer, "http")
        if ! valid_url {
               config.RexServer = "https://" + config.RexServer
        }
	configuration := openapiclient.NewConfiguration()
	configuration.Servers[0].URL = config.RexServer
	configuration.AddDefaultHeader("x-api-key", config.ApiKey)
	apiClient := openapiclient.NewAPIClient(configuration)

	// fmt.Println("REX_SERVER:", config.RexServer)
	if len(os.Args) == 1 {
		fmt.Println("rex [--list|--info|--help]| cmd [args]`")
	} else if os.Args[1] == "--list" {
		if config.ApiKey == "" {
			fmt.Println("No REX_API_KEY environment variable found - have you registered?")
		}
		appnames := listapps(*apiClient)
		for _, name := range(appnames) {
			fmt.Println(name)
		}
	} else if os.Args[1] == "--info" {
		fmt.Println("REX: Remote Execution Service v0.1.0")
		fmt.Println("REX_SERVER: ", config.RexServer)
		if config.ApiKey == "" {
			fmt.Println("No REX_API_KEY environment variable found - have you registered?")
		} else {
			fmt.Println("REX_API_KEY:", config.ApiKey)
		}
	} else if os.Args[1] == "--help" {
		fmt.Println("rex [--list|--info|--help|--register|--validate]| cmd [args]")
		fmt.Println("  --list: list available commands")
		fmt.Println("  --info: print current configuration")
		fmt.Println("  --register: register for Rex")
		fmt.Println("  --validate: validate a registration code")
		fmt.Println("  --help: print this message")
		fmt.Println("  cmd [args]: execute the command with arguments via the server")
	} else if os.Args[1] == "--register" {
		reader := bufio.NewReader(os.Stdin)
		fmt.Print("Name: ")
		name, _ := reader.ReadString('\n')
		name = name[:len(name)-1]
		fmt.Print("Email: ")
		email, _ := reader.ReadString('\n')
		email = email[:len(email)-1]
		user := register(*apiClient, name, email)
		fmt.Printf(" Validation key sent to %s\n", user.Email)

	} else if os.Args[1] == "--validate" {
		if len(os.Args) < 3 {
			log.Fatal("Error - no validation code supplied")
		}
		code := os.Args[2]
		key := validate(*apiClient, code)
		fmt.Printf(" Your API key is %s\n", key)
		fmt.Printf(" Set your REX_API_KEY environmant variable to this")

	} else {
		if config.ApiKey == "" {
			fmt.Println("No REX_API_KEY environment variable found - have you registered?")
		}

		taskid := submittask(*apiClient)
		
		ok := false
		wait_time := 500
		for !ok {
			time.Sleep(time.Duration(wait_time) * time.Millisecond)
			wait_time = wait_time * 2
			if wait_time > 10000 {
				wait_time = 10000
			}
			var status = getstatus(*apiClient, taskid)
			// fmt.Fprintf(os.Stderr, "%s", ".")
			ok = status != "pending"
		}

		taskresult := getresult(*apiClient, taskid)
		fmt.Println("Task status:", taskresult.TaskStatus)
		pr := taskresult.TaskResult
		fmt.Print(pr.GetStderr())
		
		if taskresult.TaskStatus == "finished" {
                        fmt.Print(pr.GetStdout())
			for _, file := range(pr.Outputfiles) {
				decoded, err := base64.StdEncoding.DecodeString(file.Content)
				check(err)
				dir := filepath.Dir(file.Name)
				_, err = os.Stat(dir)
				if os.IsNotExist(err) {
					err = os.MkdirAll(dir, 0755)
					check(err)
				}
				err = os.WriteFile(file.Name, decoded, 0644)
				check(err)
			}
		}
	}
}

func check(e error) {
    if e != nil {
        log.Fatal(e)
    }
}

func responsecheck(resp http.Response) {
	// if we want to check for a specific status code, we can do so here
	// for example, a successful request should return a 200 OK status
	if resp.StatusCode != http.StatusOK {
		// if the status code is not 200, we should log the status code and the
		// status string, then exit with a fatal error
	    //  he := HTTPValidationError{}
		if resp.StatusCode == http.StatusRequestEntityTooLarge {
			log.Fatal(("Error - response too large"))
		}
		if resp.StatusCode == http.StatusForbidden {
			log.Fatal("Error - cannot connect to REX_SERVER")
		}
		var he map[string]interface{}
		//  var he = HTTPErrMsg{}
		err := json.NewDecoder(resp.Body).Decode(&he)
		
	    check(err)
		log.Fatalf("Error: %s", he["detail"])
	}
}

func fileExists(filename string) bool {
    info, err := os.Stat(filename)
    if os.IsNotExist(err) {
        return false
    }
    return !info.IsDir()
}

type Config struct {
    ApiKey string
    RexServer string
}

func loadConfig() Config  {
    var config Config
    config.ApiKey = os.Getenv("REX_API_KEY")
    config.RexServer = os.Getenv("REX_SERVER")
    return config
}

func listapps(client openapiclient.APIClient) []string {
	
	resp, r, err := client.DefaultAPI.GetApplicationsApiV2AppGet(context.Background()).Execute()
	if r != nil {responsecheck(*r)}
	check(err)
	defer r.Body.Close()
	appnames := []string{}
	for _, app := range(resp.Apps) {
		appnames = append(appnames, string(app.Name))
	}
	slices.SortFunc(appnames, func(a, b string) int {
		return strings.Compare(strings.ToLower(a), strings.ToLower(b))
	})
	return appnames
}

func register(client openapiclient.APIClient, name string, email string) openapiclient.User {
	userCreate := *openapiclient.NewUserCreate(name, email)
	fmt.Printf("<%s> <%s>\n", userCreate.Name, userCreate.Email)
	resp, r, err := client.DefaultAPI.CreateUserApiV2UsersPost(context.Background()).UserCreate(userCreate).Execute()
	if r != nil {responsecheck(*r)}
	check(err)
	defer r.Body.Close()
	
	return *resp
}

func validate(client openapiclient.APIClient, registrationCode string) string {
	resp, r, err := client.DefaultAPI.ValidateApiV2ValidateRegistrationCodeGet(context.Background(), registrationCode).Execute()
	if r != nil {responsecheck(*r)}
	check(err)
	defer r.Body.Close()
	
	return resp.Apikey
}
func submittask(client openapiclient.APIClient) string {
	
	submitRequest := *openapiclient.NewSubmitRequestWithDefaults()
	submitRequest.SetCommand(os.Args[1:])
	
    for _, filename := range os.Args[1:] {
        if fileExists(filename) {
            filecontent, err := os.ReadFile(filename)
            check(err)
            filedatastr := base64.StdEncoding.EncodeToString(filecontent)
			filedata := openapiclient.NewFileData(filename, filedatastr)
			submitRequest.Inputfiles = append(submitRequest.Inputfiles, *filedata)
        }
    }
	resp, r, err := client.DefaultAPI.SubmitJobApiV2TaskPost(context.Background()).SubmitRequest(submitRequest).Execute()	
	if r != nil {responsecheck(*r)}
	check(err)
	defer r.Body.Close()
	return resp.Key
}

func getstatus(client openapiclient.APIClient, taskID string) string {
	
	resp, r, err := client.DefaultAPI.GetStatusApiV2TaskTaskIdStatusGet(context.Background(), taskID).Execute()
	if r != nil {responsecheck(*r)}
	check(err)
	defer r.Body.Close()
	return resp.Status
}

func getresult(client openapiclient.APIClient, taskID string) openapiclient.ResultResponse {

	resp, r, err := client.DefaultAPI.GetResultApiV2TaskTaskIdResultGet(context.Background(), taskID).Execute()
	if r != nil {responsecheck(*r)}
	check(err)
	defer r.Body.Close()
	return *resp
}
