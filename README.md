# REX

A simple remote execution web service

## Concept: 
install a collection of useful command-line driven tools on a certralised server, and then enable remote access to them in a way that makes them seem (almost) installed locally.

## Advantages: 
You only have to locally install a small simple web service client (works on Windows, Linux, Mac, etc.), to enable the use of any of the tools, and with API keys etc. you can control who has access to the tools.

## Disadvantages: 
The model is about moving data to the compute, rather than compute to the data (e.g. a containerization type solution), and there are some limitations on how the tools CLI works (no interactivity, all inputs explicitly defined)

## Implementation: 
Install your collection of useful tools on a `RexServer` (FastAPI-based webservice server). Install `rex` (Go executable, versions for all platforms) on your clients. For a tool on your `RexServer` that would be invoked there as:

    tool -i infile -o outfile

Run it locally as:

    rex tool -i infile -o outfile

`infile` gets sent from client to server, `tool` is run, then `outfile` is returned.

## Repository layout:

    /app        the FastAPI server app
    /client     the Go client program `rex`, also reference Python version
    

## Installation:

### Building a RexServer:
1. On your server, download/clone the repo.
2. (Optional) on the server, you only need the /app directory - delete the rest if desired.
3. Install dependencies (`pip install -r requirements.txt`).
4. Using `example.env` as a template, create a `.env` file with required environment variables.
5. Start the server (`fastapi run main.py`)
6. Configure ngnx to expose the endpoints, add TLS, etc.
7. Maybe integrate with systemd for automated lanuch/shurdown.

Once your server is up, you can add whatever tools you like to it (subject to the limitations - see below), just by placing their executables in the $REX_BINDIR directory.
**IMPORTANT**: Rex supports basic security (SSL encryption, user authorization) - but make sure $REX_BINDIR a place that only contains 'safe' tools you want to share (so not /usr/local/bin or similar).

### Obtaining and using the client (rex):
Download the version of rex for your platform from the /Downloads section of this repository. This can be done from the command line:

   wget -O rex https://bitbucket.org/claughton/rex/downloads/rex-<os>-<arch><ext>

where <os> is your operating system ("darwin", "linux", "windows", etc.), <arch> is your architecture ("amd64", "386", etc.) and <exe> is ".exe" for windows (otherwise nothing).

Once you have rex, point it at your `RexServer`:

    export REX_SERVER=<ip address of server> # e.g. https://example.com or similar

Register for an account:

    rex --register

If your registration is accepted (currently restricted to *.ac.uk email addresses), you will receive a one-time validation code by email.

Use this to validate your registration and obtain your API key:

    rex --validate <validation code>
    
Once installed, you can start using rex - use `rex --help` for details.

## Limitations

`Rex` needs to know what input files need to be sent to the `RexServer`, so they must all be explicitly defined on the command line. Tools that take as inputs files that, within them, specify other input files that are required won't work. `Rex` also won't work woth tools that need interactivity, e.g. ask questions of the user as they run and need responses.

## Questions:

Please contact [Charlie Laughton](mailto:charles.laughton@nottingham.ac.uk)
