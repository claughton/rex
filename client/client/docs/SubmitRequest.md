# SubmitRequest

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Command** | **[]string** |  | 
**Inputfiles** | Pointer to [**[]FileData**](FileData.md) |  | [optional] 

## Methods

### NewSubmitRequest

`func NewSubmitRequest(command []string, ) *SubmitRequest`

NewSubmitRequest instantiates a new SubmitRequest object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSubmitRequestWithDefaults

`func NewSubmitRequestWithDefaults() *SubmitRequest`

NewSubmitRequestWithDefaults instantiates a new SubmitRequest object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCommand

`func (o *SubmitRequest) GetCommand() []string`

GetCommand returns the Command field if non-nil, zero value otherwise.

### GetCommandOk

`func (o *SubmitRequest) GetCommandOk() (*[]string, bool)`

GetCommandOk returns a tuple with the Command field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCommand

`func (o *SubmitRequest) SetCommand(v []string)`

SetCommand sets Command field to given value.


### GetInputfiles

`func (o *SubmitRequest) GetInputfiles() []FileData`

GetInputfiles returns the Inputfiles field if non-nil, zero value otherwise.

### GetInputfilesOk

`func (o *SubmitRequest) GetInputfilesOk() (*[]FileData, bool)`

GetInputfilesOk returns a tuple with the Inputfiles field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetInputfiles

`func (o *SubmitRequest) SetInputfiles(v []FileData)`

SetInputfiles sets Inputfiles field to given value.

### HasInputfiles

`func (o *SubmitRequest) HasInputfiles() bool`

HasInputfiles returns a boolean if a field has been set.

### SetInputfilesNil

`func (o *SubmitRequest) SetInputfilesNil(b bool)`

 SetInputfilesNil sets the value for Inputfiles to be an explicit nil

### UnsetInputfiles
`func (o *SubmitRequest) UnsetInputfiles()`

UnsetInputfiles ensures that no value is present for Inputfiles, not even an explicit nil

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


