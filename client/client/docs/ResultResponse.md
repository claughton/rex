# ResultResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TaskStatus** | **string** |  | 
**TaskResult** | [**ProcessResponse**](ProcessResponse.md) |  | 

## Methods

### NewResultResponse

`func NewResultResponse(taskStatus string, taskResult ProcessResponse, ) *ResultResponse`

NewResultResponse instantiates a new ResultResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewResultResponseWithDefaults

`func NewResultResponseWithDefaults() *ResultResponse`

NewResultResponseWithDefaults instantiates a new ResultResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTaskStatus

`func (o *ResultResponse) GetTaskStatus() string`

GetTaskStatus returns the TaskStatus field if non-nil, zero value otherwise.

### GetTaskStatusOk

`func (o *ResultResponse) GetTaskStatusOk() (*string, bool)`

GetTaskStatusOk returns a tuple with the TaskStatus field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTaskStatus

`func (o *ResultResponse) SetTaskStatus(v string)`

SetTaskStatus sets TaskStatus field to given value.


### GetTaskResult

`func (o *ResultResponse) GetTaskResult() ProcessResponse`

GetTaskResult returns the TaskResult field if non-nil, zero value otherwise.

### GetTaskResultOk

`func (o *ResultResponse) GetTaskResultOk() (*ProcessResponse, bool)`

GetTaskResultOk returns a tuple with the TaskResult field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTaskResult

`func (o *ResultResponse) SetTaskResult(v ProcessResponse)`

SetTaskResult sets TaskResult field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


